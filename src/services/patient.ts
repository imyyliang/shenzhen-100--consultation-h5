import request from '@/utils/request'
import type { AddPatient, Patient } from "@/types/patient";

/** 添加患者 */
export const addPatientAPI = (patient: AddPatient) => {
  return request({ 
    url: "/patient/add", 
    method: "POST", 
    data: patient 
  });
};

/** 获取患者列表 */
export const getPatientListAPI = () => {
  return request({ url: "/patient/mylist" });
};

/** 编辑患者 */
export const editPatientAPI = (patient: AddPatient) => {
  return request({ url: "/patient/update", method: "PUT", data: patient });
};

/** 根据id，删除患者 */
export const delPatientAPI = (id: string) => {
  return request({ url: `/patient/del/${id}`, method: "DELETE" });
};