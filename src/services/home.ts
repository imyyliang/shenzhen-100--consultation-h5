import request from '@/utils/request'
import type { KnowledgeParams, DoctorParams, FollowType } from '@/types/home';

// 获取知识列表
export const getKnowledgePageAPI = (params: KnowledgeParams) => {
  return request({ url: "/patient/home/knowledge", params });
};

// 获取可关注医生
export const getDoctorPageAPI = (params: DoctorParams) => {
  return request({ url: "/home/page/doc", params });
};

// 关注医生
export const followDoctorAPI = (id: string, type: FollowType = "doc") => {
  return request({ url: "/like", method: "POST", data: { id, type } });
};