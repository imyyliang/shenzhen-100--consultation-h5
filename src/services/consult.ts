import type { OrderPreParams } from '@/types/consult'
import request from '@/utils/request'
 
/**获取所有科室 */
export const getAllDepAPI = () => request({ url: '/dep/all' })

// 上传功能
export const uploadFileAPI = (file: File) => {
  // 这里是文件对象, 不能直接用平常json的格式上传,需要根据后端需求
  // 使用 formdata 上传才行
  const fd = new FormData()
  fd.append('file', file)

  return request({
    method: 'POST',
    url: '/upload',
    data: fd
  })
}

// 2.1 请求预支付接口
/** 请求预支付数据 */
export const getPreOrderAPI = (params: OrderPreParams) => {
  return request({
    url: '/patient/consult/order/pre',
    params
  })
}

// 2.2 定义患者详情查询API
/** 查询患者详情 */
export const getPatientDetailAPI = (id: string) => {
  return request({ url: `/patient/info/${id}` })
}

/** 生成订单接口 */
export const createOrderAPI = (data: PartialConsult) => {
  return request({ url: '/patient/consult/order', method: 'POST', data })
}

// 根据订单号, 生成支付参数接口
/** 请求支付地址接口的参数类型 */
export type PayUrlParams = {
  paymentMethod: 0 | 1
  orderId: string
  payCallback: string
}

/** 获取支付地址  0 是微信  1 支付宝 */
export const getOrderPayUrlAPI = (data: PayUrlParams) => {
  return request({ url: '/patient/consult/pay', method: 'post', data })
}

/** 获取订单详情API */
export const getOrderDetailAPI = (orderId: string) => {
  return request({ url: '/patient/consult/order/detail', params: { orderId } })
}

/** 查看处方API */
export const getPrescriptionPicAPI = (id: string) => {
  return request(`/patient/consult/prescription/${id}`)
}

type EvaluateOrderParams = {
  docId: string;
  orderId: string;
  score: number;
  content: string;
  anonymousFlag: 0 | 1;
};
// 评价问诊
export const evaluateOrderAPI = (data: EvaluateOrderParams) => {
  return request({ url: "/patient/order/evaluate", method: "POST", data });
};