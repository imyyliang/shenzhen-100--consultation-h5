import request from '@/utils/request'

// 密码登录
export const loginAPI = (mobile: string, password: string) => {
  return request({
    url: '/login/password',
    method: 'post',
    data: { mobile, password }
  })
}

// 获取个人信息
// 获取个人信息
export const getUserInfoAPI = () => {
  return request({
    url: '/patient/myUser'
  })
}
