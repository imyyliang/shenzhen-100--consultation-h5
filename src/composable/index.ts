import { ref } from 'vue'
import type { Doctor } from '@/types/home'
import { followDoctorAPI } from '@/services/home'
import { showSuccessToast } from 'vant'

// 这里就是组合式api发生效果的地方, 同时也带有闭包的概念
// 我们不是暴露这个变量和函数的本体, 而是一个可以得到这个变量和函数的封装
export const useFollow = () => {
  const loading = ref(false)
  
  const follow = async (item: Doctor) => {
    loading.value = true

    // 发请求
    await followDoctorAPI(item.id)
    // 提示用户成功根据当前的关注状态决定关注成功还是取消成功
    // 原来没关注 0 的话就提示关注成功, 否则相反
    showSuccessToast(item.likeFlag === 0 ? '关注成功' : '取消关注成功')
    // 更新页面渲染
    item.likeFlag = item.likeFlag === 0 ? 1 : 0

    loading.value = false
  }

  return {loading, follow}
}

