import {defineStore} from 'pinia'
import { ref } from 'vue'
import type {PartialConsult, ConsultIllness} from '@/types/consult'
import {ConsultType} from '@/enums'

export const useConsultStore = defineStore(
  'cp-consult', 
  () => {
    // 定义一个储存问诊单的对象
    const consultForm = ref<PartialConsult>({})
    // 定义可以设置问诊单类型的函数
    const setType = (type: ConsultType) => {
      consultForm.value.type = type
    }
    // 定义函数设置医院类型
    const setIllnessType = (type: 0 | 1) => {
      consultForm.value.illnessType = type
    }
    // 定义函数设定科室
    const setDep = (id: string) => {
      consultForm.value.depId = id
    }
    // 定义储存表单数据的函数
    const setConsultIllness = (item: ConsultIllness) => {
      // 将原有数据跟新传入的数据合并, 放到储存变量中
      consultForm.value = {
        ...consultForm.value,
        ...item
      }
    }
    // 定义设置患者的函数
    const setPatient = (id: string) => {
      consultForm.value.patientId = id
    }
    // 后面问诊当的每一项, 都应该有对应的修改函数,
    // 没跳转一个页面, 就应该储存一项数据,
    // 各个页面都跳转完毕,表单就填完了

    // 写一个清空函数, 方便当前问诊付款后清空表单
    const clearConsultForm = () => {
      consultForm.value = {}
    }

    return {
      consultForm, 
      setType, 
      setIllnessType,
      setDep,
      setConsultIllness,
      setPatient,
      clearConsultForm
    }
  }, 
  { persist: true}
)