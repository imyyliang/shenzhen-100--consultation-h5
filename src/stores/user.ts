// 引入库
import { defineStore } from 'pinia'
import { ref } from 'vue'
// 如果引入的数据只用作 type 需要在前面加上标记
import type { UserType } from '@/types/user'

// 创建使用仓库的函数, 暴露导出
export const useUserStore = defineStore(
  'user',
  () => {
    // 储存数据的地方
    const user = ref<UserType>({})

    // 传入对象可以保存的函数
    const setUser = (data: UserType) => {
      user.value = data
    }

    // 清空数据的函数
    const clearUser = () => {
      user.value = {}
    }

    return {
      user, setUser, clearUser
    }
  },
  // 创建仓库的第三个参数,配置对象
  {
    persist: true
  }
)


