// 二次封装axios
import axios from 'axios'
import { useUserStore } from '@/stores/user'
import { showFailToast } from 'vant'
import router from '@/router'

export const baseURL = 'https://consult-api.itheima.net/'

// 创建axios请求实例
const request = axios.create({
  baseURL,
  timeout: 10000
})

// 请求拦截器
request.interceptors.request.use(
  function (config) {
    // 注入token
    const store = useUserStore()
    const token = store.user.token
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  function (err) {
    return Promise.reject(err)
  }
)

request.interceptors.response.use(
  function (res) {
    // 这里是请求2xx状态码的响应
    // 这不一定就是正确的数据, 比如登录请求成功发送, 但是密码错误,也是200, 需要额外处理
    if (res.data.code !== 10000) {
      showFailToast(res.data.message)
      return Promise.reject(res.data.message)
    }
    // 剥离数据, axios 默认都有一层配置数据, 真实数据都在 data 里面才是后端返回的响应
    return res.data
  },
  (err) => {
    // 这里是处理非2xx状态码的响应, 真正的报错
    // 这里的报错可能性很多, 比如 400 401 403 500 302 503
    // 我们单独对 401 进行处理
    if (err.response && err.response.status === 401) {
      // 显示错误提示
      showFailToast('登录已过期, 请重新登录')
      // 拿到仓库清理用户数据
      const store = useUserStore()
      store.clearUser()
      // 拿到路由跳转登录页
      // const router = useRouter()
      // console.log(useRouter)

      console.log(router.currentRoute.value.fullPath)

      router.push(`/login?redirect=${router.currentRoute.value.fullPath}`)
    }

    return Promise.reject(err)
  }
)

export default request
