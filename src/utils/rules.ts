// 表单校验
export const mobileRules = [
  { required: true, message: '请输入手机号' },
  { pattern: /^1[3-9]\d{9}$/, message: '手机号格式不正确' }
]

export const passwordRules = [
  { required: true, message: '请输入密码' },
  { pattern: /^\w{8,24}$/, message: '密码需8-24个字符' }
]

export const cNameRules = [
  { required: true, message: '请输入姓名' },
  { pattern: /^[\u4e00-\u9fa5]{2,8}$/, message: '姓名格式不正确'}
]

// 这里不是必须, 只是演示函数校验的方法
const idCardValidator = (val: string) => {
  const pattern = /^[1-9]\d{5}(?:18|19|20)\d{2}(?:0[1-9]|10|11|12)(?:0[1-9]|[1-2]\d|30|31)\d{3}[\dXx]$/
  return pattern.test(val)
}
export const idCardRules = [
  { required: true, message: '请输入身份证号' },
  { validator: idCardValidator, message: '身份证号格式不正确'}
]

