import { createApp } from 'vue'
import { createPinia } from 'pinia'
import persist from 'pinia-plugin-persistedstate'

import App from './App.vue'
import router from './router'

// 先引入组件库的样式
import 'vant/lib/index.css'
// 再引入自己的
import '@/styles/main.scss'
// 引入聊天室样式
import '@/styles/room.scss'

// 图标插件引入
import 'virtual:svg-icons-register'

const app = createApp(App)

// app.use(createPinia())
const pinia = createPinia()
pinia.use(persist)
app.use(pinia)

app.use(router)

app.mount('#app')
