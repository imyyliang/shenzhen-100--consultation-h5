// 登录时用户类型
export interface UserType {
  token?: string
  id?: string
  account?: string
  mobile?: string
  avatar?: string
  refreshToken?: string
}

// 个人中心用户类型数据(多了些收藏,点赞, 优惠券)
export interface OrderInfo {
  paidNumber: number
  receivedNumber: number
  shippedNumber: number
  finishedNumber: number
}

export interface ProfileType {
  couponNumber?: number
  score?: number
  likeNumber?: number
  collectionNumber?: number
  account?: string
  avatar?: string
  id?: string
  mobile?: string
  consultationInfo?: any[]
  orderInfo?: OrderInfo
}
