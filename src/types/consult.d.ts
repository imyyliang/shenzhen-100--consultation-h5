import { ConsultType, IllnessTime } from '@/enums'

// 问诊表单中图片数据类型
export interface Image {
	url: string;
	id: string;
}

// 问诊表单类型
export type Consult = {
  // 这里是问诊单需要的数据, 根据后端api需求, 有很多属性
  // 这里只做第一个演示
   /** 问诊记录ID */
   id: string
   /** 问诊类型 */
   type: ConsultType
   /** 快速问诊类型，0 普通 1 三甲 */
   illnessType: 0 | 1
   /** 科室ID */
   depId: string
   /** 疾病描述 */
   illnessDesc: string
   /** 疾病持续时间 */
   illnessTime: IllnessTime
   /** 是否就诊过，0 未就诊过  1 就诊过 */
   consultFlag: 0 | 1
   /** 图片数组 */
   pictures: Image[]
   /** 患者ID */
   patientId: string
   /** 优惠券ID */
   couponId: string
}

// 将问诊单数据类型转成全部可选, 方便逐个填写
export type PartialConsult = Partial<Consult>

// 从可选问诊单中挑选出表单页面需要的四个类型, 如果取多个, 用联合类型
export type ConsultIllness = Pick< 
  PartialConsult, 
  'illnessDesc' | 'illnessTime' | 'consultFlag' | 'pictures'
>

// 科室类型
// 子科室封装
export interface Child {
	id: string;
	name: string;
	avatar: string;
}
// 科室对象(还不是列表, 后面要加 [])
export interface Dep {
	id: string;
	name: string;
	child: Child[];
}

// 预订单接口数据(先给用户确认, 还没创建订单)
/** 问诊订单预支付信息 */
export interface OrderPreData {
  /** 实付金额 */
  actualPayment: number
  /** 优惠券抵扣 */
  couponDeduction: number
  /** 使用的优惠券id-使用优惠券时，返回 */
  couponId?: string
  /** 极速问诊类型：0普通1三甲,极速问题必须有值 */
  illnessType?: number
  /** 应付款/价格-图文或者极速的费用，极速普通10元，三甲39元 */
  payment: number
  /** 积分可抵扣 */
  pointDeduction: number
  /** 1问医生2极速问诊2开药问诊--默认是1 */
  type?: number
}

/** 请求订单支付信息的参数 */
export type OrderPreParams = Pick<OrderPreData, 'type' | 'illnessType'>

/** 问诊订单单项信息 */
export type ConsultOrderItem = Consult & {
  /** 创建时间 */
  createTime: string;
  /** 医生信息 */
  docInfo?: Doctor;
  /** 患者信息 */
  patientInfo: Patient;
  /** 订单编号 */
  orderNo: string;
  /** 订单状态 */
  status: OrderType;
  /** 状态文字 */
  statusValue: string;
  /** 类型问诊文字 */
  typeValue: string;
  /** 倒计时时间 */
  countdown: number;
  /** 处方ID */
  prescriptionId?: string;
  /** 评价ID */
  evaluateId: number;
  /** 应付款 */
  payment: number;
  /** 优惠券抵扣 */
  couponDeduction: number;
  /** 积分抵扣 */
  pointDeduction: number;
  /** 实付款 */
  actualPayment: number;
};