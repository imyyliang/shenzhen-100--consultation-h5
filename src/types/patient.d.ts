// 新增患者类型(表单数据)
export type AddPatient = {
  name: string,
  idCard: string,
  defaultFlag: number,
  gender: number,
  id?: string,
}

// 患者对象类型
export interface Patient {
	name: string;
	idCard: string;
	defaultFlag: number;
	gender: number;
	genderValue: string;
	age: number;
	id: string;
}