import { createRouter, createWebHistory } from 'vue-router'
import { useUserStore } from '@/stores/user'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
NProgress.configure({ showSpinner: false })

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // 首页布局容器
    {
      path: '/',
      // 如果只有一级路由的url, 希望重定向到首页
      redirect: '/home',
      // 一级路由容器
      component: () => import('@/views/Layout/index.vue'),
      children: [
        // 二级页面
        {
          path: 'home',
          component: () => import('@/views/Home/index.vue'),
          // 路由该有的属性, 都是路由插件默认定义好的
          // 有时候,我们希望给每个路由添加自己定义的额外信息
          // 可以通过 meta 对象来实现
          meta: { title: '首页' }
        },
        {
          path: 'article',
          component: () => import('@/views/Article/index.vue'),
          meta: { title: '文章' }
        },
        {
          path: 'notify',
          component: () => import('@/views/Notify/index.vue'),
          meta: { title: '消息' }
        },
        {
          path: 'user',
          component: () => import('@/views/User/index.vue'),
          meta: { title: '个人' }
        }
      ]
    },
    // 家庭档案
    { path: '/user/patient', component: () => import('@/views/User/PatientPage.vue') },
    // 选择极速问诊后跳转的选医院页面
    {
      path: '/consult/fast',
      component: () => import('@/views/Consult/ConsultFast.vue'),
      meta: { title: '极速问诊' }
    },
    // 选完医院类型, 跳转选科室页面
    {
      path: '/consult/dep',
      component: () => import('@/views/Consult/ConsultDep.vue'),
      meta: { title: '选择科室' }
    },
    // 选完科室跳转到详情表单填写
    {
      path: '/consult/illness',
      component: () => import('@/views/Consult/ConsultIllness.vue'),
      meta: { title: '病情描述' }
    },
    // 问诊支付页
    {
      path: '/consult/pay',
      component: () => import('@/views/Consult/ConsultPay.vue'),
      meta: { title: '问诊支付' }
    },
    // 聊天页面
    {
      path: '/room',
      component: () => import('@/views/Room/index.vue'),
    },
    // 处方支付页面
    {
      path: '/order/pay',
      component: () => import('@/views/Order/OrderPay.vue'),
      meta: { title: '药品支付' }
    },
    // 支付成功, 本来应该有详情页, 现在知识用来进行地图测试
    {
      path: '/order/pay/result',
      component: () => import('@/views/Order/OrderPayResult.vue'),
      meta: { title: '药品支付结果' }
    },
    // 登录页
    { path: '/login', component: () => import('@/views/Login/index.vue') }
  ]
})

// 对路由进行改造
router.beforeEach((to) => {
  console.log('导航守卫')

  NProgress.start()

  const store = useUserStore()
  if (store.user.token) {
    // 已登录
    // 放行
    return true
  } else {
    // 未登录
    const whiteList = ['/home', '/login']
    if (whiteList.includes(to.path)) {
      // 有些页面是无论如何都可以进入的, 比如登录, 比如注册, 比如首页, 404 等等
      // 如果是这些页面就其他才跳到登录
      return true
    } else {
      // 调到登录页
      return '/login'
    }
  }
  // 页面缺陷控制
  // 游登录就可以进没有登录就不可以
})

router.afterEach((to) => {
  console.log(to.meta.title)
  const title = '优医问诊'
  document.title = to.meta.title ? `${title} - ${to.meta.title}` : title

  NProgress.done()
})

export default router
